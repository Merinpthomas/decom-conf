<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
$con = '<h3>View Candidates</h3>';
decom_page_set_title('View Candidates');//name edit registration
$footer = new DecomPageViewFooter();
?>
<style>
.divTable {
	display: table;
}

.divTableRow {
	display: table-row;
}

.divTableCell {
	display: table-cell;
}

table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
$con = '<fieldset>';
$con .= 'Event:';
$d=0;
$con .= '<form method="POST"></br><select name="type"><option value="">-- Select Event --</option>';
$eids = decom_get_entity_ids('eventlist');
foreach($eids as $eid) {
	$obj = new DecomEntity('eventlist', $eid);
	if($obj->hasPropertyValue('Eventname', true)) {
		$ret = $obj->getPropertyValue('Eventname');
		$con .= '<option value="'.$obj->getPropertyValue('Eventname').'">'.$obj->getPropertyValue('Eventname').'<option>';
	}
}
$con .= '</select>';
$con .= "<div></br><input type=\"submit\" name=\" submit\" value=\"submit\"></div>";
if(isset($_POST['submit']))
{	
$btn=$_POST['submit'];
$b=$_POST['type'];
$ids='';
$ids = decom_get_entity_ids_by_property_value('eregister', 'Eventname',$b);
	if ($btn == 'submit'){
		$attribs = ['firstname', 'lastname','emailid','phoneno'];
		$con .= nan_table_start();
		$con .= nan_table_array_to_th( ['Sl.No','Firstname', 'Lastname','Email Id','Phone No','Action']); // TODO FIXME 		['Sl.No','Action','Name','Email','Mobile','Institution']);//details
		$cobj = new DecomClass('eregister'); //TODO remove if unused
		$attribs1 = $cobj->getAttributes();
		$class='eregister';
			for($i=0;$i<end($ids);$i++){
		    		foreach($ids as $ids) {
					$obj = new DecomEntity('eregister', $ids);
					$fname = '';
					$lname = '';
					$emailid='';
					$phoneno='';
						if($obj->hasPropertyValue('Firstname', true)) {
							$ret = $obj->getPropertyValue('Firstname');
							if(!is_array($ret))
								$fname = $ret;
						}
						if($obj->hasPropertyValue('Lastname', true)) {
							$ret = $obj->getPropertyValue('Lastname');
							if(!is_array($ret))
								$lname = $ret;
						}
						if($obj->hasPropertyValue('Emailid', true)) {
							$ret = $obj->getPropertyValue('Emailid');
							foreach($attribs1 as $a) {
								if($a->getMaxInstances() != 1)
									$emailid =implode('<br/>', $ret );
							}
						}
						if($obj->hasPropertyValue('Phoneno', true)) {
							$ret = $obj->getPropertyValue('Phoneno');
							foreach($attribs1 as $a) {
								if($a->getMaxInstances() != 1)
									$phoneno =implode('<br/>', $ret );
							}
						}
						$d=$d+1;
					$edlink = '?page=editcan&id='.$ids;
					$vwlink = '?page=canview&id='.$ids;
					$dlink='?page=deletecan&id='.$ids;
					$con .= nan_table_array_to_td([$d,$fname,$lname,$emailid,$phoneno,'<a href="'.$edlink.'">Edit</a>|<a href="'.$dlink.'">Delete</a>| <a href="'.$vwlink.'">view</a>']);
				}

		     }
		}
	}
$con .= nan_table_close();
decom_page_set_content($con);
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
?>
