<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/people/auth.php');
require_once($DELIBDIR.'/php/people/person.php');
require_once($DELIBDIR.'/php/view.php');
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
			/*
			$v = new DecomView();
			$v->load('entity', 'person', 0);
			echo $v->render();*/
			$cobj = new DecomClass('eregister');
			$attribs = $cobj->getAttributes();
			$inputFields = [];
			$inputFieldLabels = [];
			$inputFieldTypes = [];
			$inputFieldsReq = [];
			$customInput   = [];
			$defaultValues = [];
			$action = '?page=editprocess';
			$method = 'POST';
			foreach($attribs as $a) {
				$inputFields[]      = $a->getName();
				$inputFieldLabels[] = $a->getName();
				$inputFieldTypes[]  = ($a->getType() == 'int')? 'number': 'text';
				$inputFieldsReq[]   = $a->getRequired();
			}
			$eids = decom_get_entity_ids('eregister');
			foreach($eids as $eid) {
				$obj = new DecomEntity('eregister', $eid);
				foreach($inputFields as $f){
					if($obj->hasPropertyValue($f)){
						$defaultValues[$f] = $obj->getPropertyValue($f);
					}
				}
			}
$frm=nan_generate_form($inputFields,$inputFieldLabels,$inputFieldTypes,$inputFieldsReq,$action,$method,$customInput,$defaultValues);
			$con =$frm;			
?> 

