<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
decom_page_set_title('Upload Paper');
$con ='<h3>Upload Paper</h3>';
$con .= '<fieldset>';
$con .= '<select name="type"><option value="">-- Select Department --</option>';
$diRoot = new DecomInstitute(2);
$schools = $diRoot->getChildrenIds();
	foreach($schools as $school) {
		$diSchool = new DecomInstitute($school);
		$depts = $diSchool->getChildrenIds();
			foreach($depts as $dept) {
				$diDept = new DecomInstitute($dept);
				$con .= '<option value="'.$diDept->getPropertyValue('name').'">'.$diDept->getPropertyValue('name').'<option>';
			}
	}
$con .= '</select>';
$con .= '<select name="type"><option value="">-- Select Event --</option>';
$eids = decom_get_entity_ids('events');
foreach($eids as $eid) {
	$obj = new DecomEntity('events', $eid);
	if($obj->hasPropertyValue('name', true)) {
		$ret = $obj->getPropertyValue('name');
				$con .= '<option value="'.$obj->getPropertyValue('name').'">'.$obj->getPropertyValue('name').'<option>';
			}
	}
$con .= '</select>';
$frm =nan_generate_form(['name','mob','email','paperTitle'],['Name','Mobile Number','Email Id','Paper Title'],['text','int','text','text'],[true,true,true,true],"");
$con .="Upload Abstract";
$con .= "<div></br><input type=\"file\" name=\"image\"></div>";
$con .=$frm;
$footer = new DecomPageViewFooter();
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
decom_page_set_content($con);
?>

