<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/page.php");
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/people/person.php');
require_once($DELIBDIR.'/php/entity.php');
 	
$inputFields= ['uname', 'passwd', 'passwdConfirm'];
$inputFieldsReq = [true, true, true];
$allSet = true;
foreach($inputFields as $i => $f) {
	if($inputFieldsReq[$i] == true && !isset($_POST[$f])) {
			$allSet = false;
			decom_page_add_error_message('Required field <i>'.$f.'</i> not set.', 'Error creating new user');
			break;
	}
}
if($allSet){
	$errttl = 'Error creating new login account';
  if($_POST['passwdConfirm'] != $_POST['passwd']) {
		decom_page_add_error_message('Password confirmation mismatch.', $errttl);
	}
	else
 	{
  	try {   
    	$name =$_POST['uname'];
    	$an = decom_create_entity('person',[['first_name',$name ]]);
    	echo $an;
			if(is_a($an, 'DecomError')){
				$con .= '<p>Error creating entity: '.$ret->getMessageHtml().'</p>';
			}
			else{
				$pobj = new DecomPerson($an); // TODO err
		  		$ret  = $pobj->createLogin($_POST['uname'], $_POST['passwd']);
		  		echo $ret;
				if(is_a($ret, 'DecomError'))
					$con .= '<p>Error creating login: '.$ret->getMessageHtml().'</p>';
				else
					decom_page_add_message('New login added successfully.');
			}
		}
		catch(Exception $e) {
			decom_page_add_error_message($e->getMessage(), $errttl);
		}
	}
}
/*else {
	echo "err";
				// TODO err
}*/
?> 
