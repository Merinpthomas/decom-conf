<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/class.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once("$DELIBDIR/php/nan/table.php");
$con = '<h3>Add Events</h3>';
decom_page_set_title('Add Events');
$con .= '<fieldset>';
$con .='Event Category : ';
$inputFields = ['AddConference','AddWorkshop','AddSeminar'];
$con .= '<select id="activitySelector"name="type"><option value="">-- None --</option>';	
$con .= '<option value="'.$inputFields[0].'">'.$inputFields[0].'</option>';
$con .= '<option value="'.$inputFields[1].'">'.$inputFields[1].'</option>';
$con .= '<option value="'.$inputFields[2].'">'.$inputFields[2].'</option>';
$con .= '</select>';
$con .='<h1 id="text"></h1>';
$con  .='<script>
var activities = document.getElementById("activitySelector");
activities.addEventListener("click", function() {
var selector = document.getElementById("activitySelector");
var value = selector[selector.selectedIndex].value;
if(value=="AddConference")
{
}
});
</script>';
$footer = new DecomPageViewFooter();
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
decom_page_set_content($con);
?>

