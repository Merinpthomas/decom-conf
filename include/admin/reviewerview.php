<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
$con = '<h3>View Reviewer</h3>';
decom_page_set_title('View Reviewer');//name edit registration
$footer = new DecomPageViewFooter();
?>
<style>
.divTable {
	display: table;
}

.divTableRow {
	display: table-row;
}

.divTableCell {
	display: table-cell;
}

table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
$con .= nan_table_start();
$con .= nan_table_array_to_th( ['Sl.No','Reviewname','Action']); // TODO FIXME ['Sl.No','Action','Name','Email','Mobile','Institution']);//details
$cobj = new DecomClass('rev'); //TODO remove if unused
$attribs1 = $cobj->getAttributes();
$class='rev';
$id=1;
$eids = decom_get_entity_ids('rev');
foreach($eids as $eid) {
	$obj = new DecomEntity('rev', $eid);
	$rname = '';
	if($obj->hasPropertyValue('Reviewname', true)) {
		$ret = $obj->getPropertyValue('Reviewname');

		if(!is_array($ret))
			$rname = $ret;
	}
	
	$rvwlink = '?page=viewreviewer&id='.$eid;
	
	$con .= nan_table_array_to_td([$id,$rname,'<a href="'.$rvwlink.'">view</a>']);
	$id=$id+1;
}
$con .= nan_table_close();
decom_page_set_content($con);
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
?>
