<?php
/* This file is part of libdecom.
 * Copyright (C) 2019 Nandakumar Edamana
 * Started on 2019-04-27, file separated on 2019-05-18.
 */

if(!isset($_GET['id'])) { // TODO pass via $_POST?
	decom_page_add_error_message(_('Entity ID not specified.'), _('Error deleting entity'));
}
else {
	$ret = decom_remove_entity('eregister', $_GET['id']);
	if($ret === true)
		decom_page_add_message(_('Entity deleted successfully.'));
	else
		decom_page_add_error_message($ret->getMessageHtml(), _('Error deleting entity'));
}
?>
