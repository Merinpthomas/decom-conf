<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/menu.php');
require_once("$DELIBDIR/php/nan/form.php");
require_once($DELIBDIR.'/php/people/person.php');
$editMode=true;
$kvp = [];
$cobj = new DecomClass('eregister');
$attribs = $cobj->getAttributes();
foreach($attribs as $a) {
	$aname = $a->getName();
	if(isset($_POST[$aname])) {
		$kvp[] = [$aname, $_POST[$aname]];
	}
	else if($a->getRequired()) {
		decom_page_add_error_message('Required field <i>'.$aname.'</i> is not set.');
		$kvp = null;
		break;
	}
}
if($kvp !== null) {
	if($editMode)
		$ret = decom_edit_entity('eregister',$_GET['id'], $kvp);
	else
		$ret = decom_create_entity('eregister', $kvp);
	if(is_a($ret, 'DecomError'))
		$con .= '<p>Error creating/updating entity: '.$ret->getMessageHtml().'</p>'; // TODO std. msgbox
	else
		decom_page_add_message('Updated Successfully.');
}
?> 




