<style>
.divTable {
	display: table;
}
.divTableRow {
	display: table-row;
}
.divTableCell {
	display: table-cell;
}
table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
require_once($DELIBDIR.'/php/cms/gateway.php');
require_once("$DELIBDIR/php/nan/form.php");
decom_page_init();
$footer = new DecomPageViewFooter();
$nav =  new DecomMenu();
$subper=new DecomMenu();
$subm = new DecomMenu();
$subper1=new DecomMenu();
switch($_SESSION['utype'])
{	
	case 'admin':  
		decom_page_set_title('List of Events');
        	$subper->addItem(
                	new DecomMenuItem('Add Conferences','index.php?page=AddConferences', 'Add Conferences page'));
        	$subper->addItem(
                	new DecomMenuItem('Reviewer','/index.php', 'Reviewer page',$subper1));
        	$subper1->addItem(
                	new DecomMenuItem('Add Reviewer','index.php?page=reviewer','Add Reviewer page'));
       		$subper1->addItem(
                	new DecomMenuItem('Add Reviewer As User','index.php?page=adduserform','Add Reviewer As User page'));
        	$subper1->addItem(
                	new DecomMenuItem('View Reviewer','index.php?page=reviewerview','View Reviewer page'));
		$subper->addItem(
                	new DecomMenuItem('View Abstract','index.php?page=Abstract','Abstract page')); 
        	$subper->addItem(
                	new DecomMenuItem('Opted Papers','index.php?page=Papers','Opted Papers page'));
        	$subm->addItem(
			new DecomMenuItem('Conferences', '/index.php', 'Conferences page',$subper));
		$subper1=new DecomMenu();
		$subper1->addItem(
                	new DecomMenuItem('Add seminar & Workshops','index.php?page=AddsemWorkshops', 'Add seminar & Workshop page'));	
		$subm->addItem(
			new DecomMenuItem('Workshops / Seminars', '/index.php', 'Conferences page',$subper1));  
		$con .= '<u><h3>Conferences/Seminars/Workshop</h3></u>';//date title details(brouctue) edit delete view(brouchure) schedule
		$con .= nan_table_start();
		$con .= nan_table_array_to_th(['Sl.No','Department','Name','Type','Date','Upload','Action']);
		$cobj = new DecomClass('events'); //TODO remove if unused
		$attribs1 = $cobj->getAttributes();
		$class='events';
		$id=1;
		$eids = decom_get_entity_ids('eventlist');
		foreach($eids as $eid) {
			$obj = new DecomEntity('eventlist', $eid);
			$name = '';
			$department='';
			$type='';
			$date='';
			if($obj->hasPropertyValue('Department', true)) {
				$ret1 = $obj->getPropertyValue('Department');

				if(!is_array($ret1))
					$department = $ret1;
			}
			if($obj->hasPropertyValue('Eventname', true)) {
				$ret = $obj->getPropertyValue('Eventname');
		
				if(!is_array($ret))
					$name = $ret;
			}
			if($obj->hasPropertyValue('Eventtype', true)) {
				$ret = $obj->getPropertyValue('Eventtype');

				if(!is_array($ret))
					$type = $ret;
			}
			if($obj->hasPropertyValue('Date', true)) {
				$ret1 = $obj->getPropertyValue('Date');

				if(!is_array($ret1))
					$date = $ret1;
			}
		$edlink = 'index.php?page=editconevent&event='.$name.'&type='.$type;
		$vmlink= 'index.php?page=eventconview&event='.$name.'&type='.$type;
		$dlink = 'index.php?page=deleteconevent&event='.$name.'&type='.$type;
		$con .= nan_table_array_to_td([$id,$department,
		$name,$type,$date,'<a href="'.$edlink.'">Upload Brouchure</a>|<a href="'.$edlink.'">Upload Schedule</a>','<a href="'.$edlink.'">Edit</a>|<a href="'.$dlink.'">Delete</a>|<a href="'.$vmlink.'">View</a>']); // TODO implement schedule broucher
		$id =$id+1;
		}
		$con .= nan_table_close();
		break;
	case 'reviewer': 
		$subper1->addItem(
                	new DecomMenuItem('View Profile','index.php?page=viewProfile','View profile page'));
        	$subper1->addItem(
                	new DecomMenuItem('Edit Profile ','index.php?page=editprofile','Edit Profile page'));
                $subm->addItem(
		new DecomMenuItem('Profile', '/index.php', 'Profile page',$subper1));
		$con = '<fieldset>';
		$con .= 'Event:';
		$id=0;
		$con .= '<form method="POST"></br><select name="type"><option value="">-- Select Event --</option>';
		$eids = decom_get_entity_ids('eventlist');
		foreach($eids as $eid) {
			$obj = new DecomEntity('eventlist', $eid);
			if($obj->hasPropertyValue('Eventname', true)) {
				$ret = $obj->getPropertyValue('Eventname');
				$con .= '<option value="'.$obj->getPropertyValue('Eventname').'">'.$obj->getPropertyValue('Eventname').'<option>';
			}
		}
		$con .= '</select>';
		$con .= "<div></br><input type=\"submit\" name=\" submit\" value=\"submit\"></div>";
		if(isset($_POST['submit']))
		{	
			$btn=$_POST['submit'];
			$b=$_POST['type'];
			$ids='';
			$ids1='';
			$ids1 = decom_get_entity_ids_by_property_value('eregister', 'Eventname',$b);
			if ($btn == 'submit'){
				$con .= nan_table_start();
				$con .= nan_table_array_to_th( ['Sl.No','Firstname', 'ViewAbstract','Action']); // TODO FIXME 					['Sl.No','Action','Name','Email','Mobile','Institution']);//details
				$cobj = new DecomClass('eregister'); //TODO remove if unused
				$attribs1 = $cobj->getAttributes();
				for($i=0;$i<end($ids1);$i++){
					$ids= decom_get_entity_ids_by_property_value('eregister', 'Paperpresent','yes');
		    			for($i=0;$i<end($ids1);$i++)
		    			{
		    			//echo $ids[$i];
		    			}
					foreach($ids as $ids) {
						$obj = new DecomEntity('eregister', $ids);
						$fname = '';
						$lname = '';
						$emailid='';
						$phoneno='';
						$eventname='';
						if($obj->hasPropertyValue('Firstname', true)) {
							$ret = $obj->getPropertyValue('Firstname');

							if(!is_array($ret))
								$name = $ret;
						}
					$ablink= 'reviewerindex.php?page=Abstract1&id='.$eid;
					$con .= nan_table_array_to_td([$eid,
					$name,'<a href="'.$ablink.'">ViewAbstract</a>','<a href="'.$ablink.'">Select</a>']); // TODO implement schedule broucher
					}
				}
			}	
		}
		break;
	default:	echo "hello";
				
}
if(in_array($_SESSION['utype'], ['admin'])){
		$nav->addItem(
	   		new DecomMenuItem('Home', '/index.php', 'Home page'));
		$nav->addItem(
	   		new DecomMenuItem('View Candidates','index.php?page=Candidates', 'View Candidates'));// edit delete view details schedule 
		$nav->addItem(
	   		new DecomMenuItem('Opted Accomodation','index.php?page=Accomodation','Opted Accomodation'));
		$nav->addItem(
	   		new DecomMenuItem('Logout', 'index.php?page=logout', 'Logout page'));        
}
if(in_array($_SESSION['utype'], ['reviewer'])){
	$nav->addItem(
	   	new DecomMenuItem('Home','/index.php', 'Home Page'));
	$nav->addItem(
	   	new DecomMenuItem('Event Details','index.php?page=Eventdetails', 'Event Details Page'));
	$nav->addItem(
	   	new DecomMenuItem('Department Details','index.php?page=depdetails', 'Department Details Page'));   	
        $nav->addItem(
	   	new DecomMenuItem('Logout', 'index.php?page=logout', 'Logout page'));// table name view details // abstract
	//$con ='Conference list';  
}
$page = isset($_GET['page'])? $_GET['page']: 'home';
$ret  = decom_autoinclude($page, '../include/'.$_SESSION['utype']);
if(is_a($ret, 'DecomError')) {
	decom_page_add_error_message($ret->getMessageHtml());
}
else {
	foreach($ret as $path)
		include($path);
}
decom_page_set_navbar($nav);
decom_page_set_side_menu($subm);
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
decom_page_set_content($con);
?>

