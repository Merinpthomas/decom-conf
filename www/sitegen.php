<?php
	require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
	//require_once('config.php');
	require_once("$DELIBDIR/php/views/page.php");
	//require_once("$DELIBDIR/php/dept.php");
	require_once("$DELIBDIR/php/menu.php");
	require_once("$DELIBDIR/php/navigator.php");
	require_once($DELIBDIR.'/php/inst.php');
	require_once($DELIBDIR.'/php/sites.php');
	require_once($DELIBDIR.'/php/site.php');
	require_once($DELIBDIR.'/php/entity.php');
	require_once($DELIBDIR.'/php/view.php');
	require_once($DELIBDIR.'/php/people/person.php');


	decom_page_init();
        decom_page_set_header_logo_url('EventPics/common/uoc-header-transp.png');

	$cont = '';
	$footer = new DecomPageViewFooter();

	if(!isset($_GET['inst'])) {
		$diRoot = new DecomInstitute(2);
		$schools = $diRoot->getChildrenIds();

		$menu = new DecomMenu();
		
		foreach($schools as $school) {
			$submenu = new DecomMenu();
			$diSchool = new DecomInstitute($school);
			$nav = new DecomNavigator();
			$depts = $diSchool->getChildrenIds();

			foreach($depts as $dept) {
				$diDept = new DecomInstitute($dept);
				$nav->setParameter('inst',$dept);
				$submenu->addItem(
					new DecomMenuItem(
						$diDept->getPropertyValue('name'),   // text
						$nav->toUrl(),                // url
						$diDept->getPropertyValue('name'))); // hint
			}

			$menu->addItem(
				new DecomMenuItem(
					$diSchool->getPropertyValue('name'),
					'#',
					$diSchool->getPropertyValue('name'),$submenu));
		}

		$cont .= $menu->toHtml('_deBlockList _deBlockListMinH10em _deBlockListW95 _deCols3');
	}	
	else
	{
	$did = $_GET['inst'];     //TODO error check        
		
		$iobj = new DecomInstitute($did);
		$v = new DecomView();
		
		$title = $iobj->getPropertyValue('name');
		decom_page_set_title($title);
		/*$site = $iobj->getPropertyValue('site');
		$sobj = new DecomSite($site);*/
		
		
		
		$eids = decom_get_entity_ids_by_property_value('eventlist','Department',$title);
		$menu1 = new DecomMenu();
		foreach($eids as $id) {
			//$submenu = new DecomMenu();
			//$diSchool = new DecomInstitute($school);
			$nav = new DecomNavigator();
			//$depts = $diSchool->getChildrenIds();
			
			$obj = new DecomEntity('eventlist', $id);
						$ename = '';
						
							if($obj->hasPropertyValue('Eventname', true)) {
								$ret = $obj->getPropertyValue('Eventname');
								if(!is_array($ret))
									$ename = $ret;
									}
			$cont .= '<h3>Event List</h3>';
			
			$nav->setParameter('page','ori.php');
									
			$menu1->addItem(
				new DecomMenuItem(
					$obj->getPropertyValue('Eventname'),
					$nav->toUrl(),
					$obj->getPropertyValue('Eventname')));
							
					//echo $obj->getPropertyValue('Eventname');		
			/*$ids = decom_get_entity_ids_by_property_value('event','Eventname',$ename);
			foreach($ids as $id1) {			
				$obj = new DecomEntity('event', $id1);
					$v = new DecomView();
		$v->load('entity','event',$id1);
		$cont .= $v->render();
			
			}	*/
							
							
							
		}
			$cont .= $menu1->toHtml();
			
			/*foreach($depts as $dept) {
				$diDept = new DecomInstitute($dept);
				$nav->setParameter('inst',$dept);
				$submenu->addItem(
					new DecomMenuItem(
						$diDept->getPropertyValue('name'),   // text
						$nav->toUrl(),                // url
						$diDept->getPropertyValue('name'))); // hint
			}

			$menu->addItem(
				new DecomMenuItem(
					$diSchool->getPropertyValue('name'),
					'#',
					$diSchool->getPropertyValue('name'),$submenu));*/
		
		
		
		
		
		
		
		
	
		$footer->setCustomHtml('<p>Copyright (C) 2019 Calicut university.</p>');
	}
	
	decom_page_set_footer($footer);
	decom_page_set_content($cont);
	decom_page_display();
?>
	
	
