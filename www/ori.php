<style>
.divTable {
	display: table;
}
.divTableRow {
	display: table-row;
}
.divTableCell {
	display: table-cell;
}
table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
decom_page_init();
$footer = new DecomPageFooter();
decom_page_set_title('List of Events');
$nav =  new DecomMenu();
	$nav->addItem(
	   	new DecomMenuItem('Home', '/index.php', 'Home page'));
	$nav->addItem(
	   	new DecomMenuItem('View Candidates','index.php?page=Candidates', 'View Candidates'));// edit delete view details schedule 
	$nav->addItem(
	   	new DecomMenuItem('Opted Accomodation','index.php?page=Accomodation','Opted Accomodation'));
	$nav->addItem(
	   	new DecomMenuItem('Logout', 'index.php?page=logout', 'Logout page'));        
        $subper=new DecomMenu();
	$subper->addItem(
                new DecomMenuItem('Add Conferences','index.php?page=AddConferences', 'Add Conferences page'));
        $subper1=new DecomMenu();
	$subper->addItem(
                new DecomMenuItem('Reviewer','/index.php', 'Reviewer page',$subper1));
        $subper1->addItem(
                new DecomMenuItem('Add Reviewer','index.php?page=reviewer','Add Reviewer page'));
       	$subper1->addItem(
                new DecomMenuItem('Add Reviewer As User','index.php?page=adduserform','Add Reviewer As User page'));
        $subper1->addItem(
                new DecomMenuItem('View Reviewer','index.php?page=reviewerview','View Reviewer page'));
	$subper->addItem(
                new DecomMenuItem('View Abstract','index.php?page=Abstract','Abstract page')); 
        $subper->addItem(
                new DecomMenuItem('Opted Papers','index.php?page=Papers','Opted Papers page'));
        $subm = new DecomMenu();
	$subm->addItem(
		new DecomMenuItem('Conferences', '/index.php', 'Conferences page',$subper));
	$subper1=new DecomMenu();
	$subper1->addItem(
                new DecomMenuItem('Add seminar & Workshops','index.php?page=AddsemWorkshops', 'Add seminar & Workshop page'));	
	$subm->addItem(
		new DecomMenuItem('Workshops / Seminars', '/index.php', 'Conferences page',$subper1));  
$con = '<u><h3>Conferences/Seminars/Workshop</h3></u>';//date title details(brouctue) edit delete view(brouchure) schedule
$con .= nan_table_start();
$con .= nan_table_array_to_th(['Sl.No','Department','Name','Type','Date','Upload','Action']);
$cobj = new DecomClass('events'); //TODO remove if unused
$attribs1 = $cobj->getAttributes();
$class='events';
$eids = decom_get_entity_ids('eventlist');
foreach($eids as $eid) {
	$obj = new DecomEntity('eventlist', $eid);
	$name = '';
	$department='';
	$type='';
	$date='';
	if($obj->hasPropertyValue('Department', true)) {
		$ret1 = $obj->getPropertyValue('Department');

		if(!is_array($ret1))
			$department = $ret1;
	}
	if($obj->hasPropertyValue('Eventname', true)) {
		$ret = $obj->getPropertyValue('Eventname');

		if(!is_array($ret))
			$name = $ret;
	}
	if($obj->hasPropertyValue('Eventtype', true)) {
		$ret = $obj->getPropertyValue('Eventtype');

		if(!is_array($ret))
			$type = $ret;
	}
	if($obj->hasPropertyValue('Date', true)) {
		$ret1 = $obj->getPropertyValue('Date');

		if(!is_array($ret1))
			$date = $ret1;
	}
	$edlink = 'index.php?page=editconevent&id='.$eid;
	$vmlink= 'index.php?page=eventconview&id='.$eid;
	$dlink = 'index.php?page=deleteconevent&id='.$eid;
	$con .= nan_table_array_to_td([$eid,$department,
		$name,$type,$date,'<a href="'.$edlink.'">Upload Brouchure</a>|<a href="'.$edlink.'">Upload Schedule</a>','<a href="'.$edlink.'">Edit</a>|<a href="'.$dlink.'">Delete</a>|<a href="'.$vmlink.'">View</a>']); // TODO implement schedule broucher
}
$con .= nan_table_close();
//$con .= $subm->toHtml();
//TODO
$page=$_GET['page'];
switch($page){
	case 'AddEvent':include($_SERVER['DOCUMENT_ROOT'].'/../include/AddEvent.php');
        	break;
       // case 'NewEvent':include($_SERVER['DOCUMENT_ROOT'].'/../include/NewEvent.php');
            //  break;
	case 'AddConferences':include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/AddConferences.php');
                break;
	case 'AddsemWorkshops':include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/AddsemWorkshops.php');
                break;
        case 'AddSchedule' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/AddSchedule.php');
                break;
        case 'Candidates' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/Candidates.php');
                break;
	case 'Abstract' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/Abstract.php');
                break;
	case 'Papers' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/Papers.php');
                break;
        case 'Accomodation' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/Accomodation.php');
                break;
        case 'register' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/register.php');
                break;
      /*case 'upload' :include($_SERVER['DOCUMENT_ROOT'].'/../include/upload.php');
                break;*/
        case 'logout' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/logout.php');
                break;
        case 'testedit' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/testedit.php');
                break;
        case 'upbrochure' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/upbrochure.php');
                break;
        case 'view' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/view.php');
                break;
        case 'editcan' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/editcan.php');
                break;
        case 'canview' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/canview.php');
                break;
        case 'editcanprocess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/editcanprocess.php');
                break;
        case 'deletecan' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/deletecan.php');
                break;
        case 'eventconview' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/eventconview.php');
                break;
        case 'deleteconevent' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/deleteconevent.php');
                break;
        case 'editconevent' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/editconevent.php');
                break;
        case 'editconprocess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/editconprocess.php');
                break;   
        case 'acinactive' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/acinactive.php');
                break;  
        case 'reviewer' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/reviewer.php');
                break; 
        case 'addsemworkProcess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/addsemworkProcess.php');
                break;
        case 'addconfProcess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/addconfProcess.php');
                break;     
        case 'ViewAbstract' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/ViewAbstract.php');
                break;
        case 'reviewprocess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/reviewprocess.php');
                break;   
        case 'viewreviewer' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/viewreviewer.php');
                break; 
        case 'reviewerview' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/reviewerview.php');
                break; 
        case 'adduserform' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/adduserform.php');
                break; 
        case 'adduserProcess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/admin/adduserProcess.php');
                break; 
}
decom_page_set_navbar($nav);
decom_page_set_side_menu($subm);
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
decom_page_set_content($con);
decom_page_display();
//}
?>
