 <?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once($DELIBDIR.'/php/people/auth.php');
require_once($DELIBDIR.'/php/people/person.php');
	session_start();
	if(isset($_SESSION['utype'])){
		header("Location:index.php");
	}
	else { // Not logged in already
		$uname=$_POST['uname'];
		echo $uname;
 		$passwd=$_POST['passwd'];
 		$ret = decom_auth($uname,$passwd);
 		if(is_a($ret, 'DecomError')) {  // Failure
 			//header("Location:index.php");
			echo '<p>Error: '.$ret->getMessage().'. <a href="./">Click here</a> to go back and try again.</p>';
	  }
	  else {                          // Success
			$_SESSION['uname'] = $uname;
			$_SESSION['pid'] = $ret;
			if($_SESSION['uname']=='admin')
				$_SESSION['utype'] = 'admin';
			$diUser = new DecomPerson($_SESSION['pid']);
			$did = $diUser->getRelativeFirst('head_of');
			if($did == NULL|| $did == FALSE ){
					$did = $diUser->getRelativeFirst('faculty_of');
					if($did == NULL|| $did == FALSE ){
						$did = $diUser->getRelativeFirst('staff_of');
						if($did == NULL|| $did == FALSE )
								echo '<p>Invalid User. Redirecting; please wait...</p>';
						else
							$_SESSION['utype'] = 'staff';
					}
					else
						$_SESSION['utype'] = 'faculty';
			}
			else
					$_SESSION['utype'] = 'hod';
			
			$did = $diUser->getRelativeFirst('Reviewer_as_');
			if($did == NULL|| $did == FALSE )
					echo '<p>Invalid User. Redirecting; please wait...</p>';
			else
					$_SESSION['utype'] = 'reviewer';
	    //$_SESSION['utype'] = 'hod'; // TODO FIXME make automatic
	    echo '<p>Login successful. Redirecting; please wait...</p>';
      header("Location: /");
    }
  }
?>
