<style>
.divTable {
	display: table;
}
.divTableRow {
	display: table-row;
}
.divTableCell {
	display: table-cell;
}
table { border-collapse: collapse }
table th, table td { border: 1px solid #888; padding: 0.5em }
</style>
<?php
require_once($_SERVER['DOCUMENT_ROOT'].'/delocalconf.php');
require_once("$DELIBDIR/php/views/page.php");
require_once($DELIBDIR.'/php/menu.php');
require_once($DELIBDIR.'/php/nan/table.php');
require_once($DELIBDIR.'/php/db.php'); // TODO move to top?
require_once($DELIBDIR.'/php/class.php');
require_once($DELIBDIR.'/php/entity.php');
require_once("$DELIBDIR/php/navigator.php");
require_once($DELIBDIR.'/php/inst.php');
require_once($DELIBDIR.'/php/sites.php');
require_once($DELIBDIR.'/php/site.php');
decom_page_init();
$footer = new DecomPageViewFooter();
decom_page_set_title('Conference Papers');
//$con = 'Reviewer Home Page';
$subper=new DecomMenu();
$sub=new DecomMenu();
$nav =  new DecomMenu();
  $subm = new DecomMenu();
     $subper1=new DecomMenu();
     // $sub->addItem(
         //     new DecomMenuItem('View Abstract','reviewerindex.php?page=Abstract1','Abstract page'));
        $nav->addItem(
	   	new DecomMenuItem('Home','/reviewerindex.php', 'Home Page'));
	$nav->addItem(
	   	new DecomMenuItem('Event Details','reviewerindex.php?page=Eventdetails', 'Event Details Page'));
	$nav->addItem(
	   	new DecomMenuItem('Department Details','reviewerindex.php?page=depdetails', 'Department Details Page'));   	
        $nav->addItem(
	   	new DecomMenuItem('Logout', '/reviewerindex.php', 'Logout page'));// table name view details // abstract
	//$con ='Conference list';  
	$subm->addItem(
		new DecomMenuItem('Profile', '/reviewerindex.php', 'Profile page',$subper1));
	$subper1->addItem(
                new DecomMenuItem('View Profile','reviewerindex.php?page=viewProfile','View profile page'));
        $subper1->addItem(
                new DecomMenuItem('Edit Profile ','reviewerindex.php?page=editprofile','Edit Profile page'));
	$con = '<fieldset>';
$con .= 'Event:';
$id=0;
$con .= '<form method="POST"></br><select name="type"><option value="">-- Select Event --</option>';
$eids = decom_get_entity_ids('eventlist');
foreach($eids as $eid) {
	$obj = new DecomEntity('eventlist', $eid);
	if($obj->hasPropertyValue('Eventname', true)) {
		$ret = $obj->getPropertyValue('Eventname');
		$con .= '<option value="'.$obj->getPropertyValue('Eventname').'">'.$obj->getPropertyValue('Eventname').'<option>';
	}
}
$con .= '</select>';
$con .= "<div></br><input type=\"submit\" name=\" submit\" value=\"submit\"></div>";

	if(isset($_POST['submit']))
	{	
		$btn=$_POST['submit'];
		$b=$_POST['type'];
		$ids='';
		$ids1='';
		$ids1 = decom_get_entity_ids_by_property_value('eregister', 'Eventname',$b);
		if ($btn == 'submit'){
			$con .= nan_table_start();
			$con .= nan_table_array_to_th( ['Sl.No','Firstname', 'ViewAbstract','Action']); // TODO FIXME 				['Sl.No','Action','Name','Email','Mobile','Institution']);//details
			$cobj = new DecomClass('eregister'); //TODO remove if unused
			$attribs1 = $cobj->getAttributes();
			for($i=0;$i<end($ids1);$i++){
				$ids= decom_get_entity_ids_by_property_value('eregister', 'Paperpresent','yes');
		    		for($i=0;$i<end($ids1);$i++)
		    		{
		    			//echo $ids[$i];
		    		}
			
		    		foreach($ids as $ids) {
					$obj = new DecomEntity('eregister', $ids);
					$fname = '';
					$lname = '';
					$emailid='';
					$phoneno='';
					$eventname='';
						if($obj->hasPropertyValue('Firstname', true)) {
						$ret = $obj->getPropertyValue('Firstname');

						if(!is_array($ret))
							$name = $ret;
				}
	$ablink= 'reviewerindex.php?page=Abstract1&id='.$eid;
	$con .= nan_table_array_to_td([$eid,
		$name,'<a href="'.$ablink.'">ViewAbstract</a>','<a href="'.$ablink.'">Select</a>']); // TODO implement schedule broucher
}
}
}
}
$page=$_GET['page'];
switch($page){
case 'Abstract1' :
	include($_SERVER['DOCUMENT_ROOT'].'/../include/Abstract1.php');// select best abstract
                break;
case 'Eventdetails' :include($_SERVER['DOCUMENT_ROOT'].'/../include/reviewer/Eventdetails.php');
                break;
case 'depdetails' :include($_SERVER['DOCUMENT_ROOT'].'/../include/reviewer/depdetails.php');
                break;
case 'viewProfile' :include($_SERVER['DOCUMENT_ROOT'].'/../include/reviewer/viewProfile.php');
                break;
case 'editprofile' :include($_SERVER['DOCUMENT_ROOT'].'/../include/reviewer/editprofile.php');
                break;
case 'editprofileprocess' :include($_SERVER['DOCUMENT_ROOT'].'/../include/reviewer/editprofileprocess.php');
                break;              
}
$con .= nan_table_close();
decom_page_set_navbar($nav);
decom_page_set_side_menu($subm);
$footer->setCustomHtml('<p align=center>Copyright (C) 2019 Calicut university.</p>');
decom_page_set_footer($footer);
decom_page_set_content($con);
decom_page_display();
?>
